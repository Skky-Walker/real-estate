from models.deal import Rent, Sale
from models.estate import Apartment, House, Store
from models.supervisor import Supervisor


class Advertisement:

    def __init__(self, **kwargs):
        Supervisor.estates_list.append(self)
        super().__init__(**kwargs)


class ApartmentRent(Apartment, Rent, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        apartment_prompt = Apartment.prompt(**kwargs)
        rent_prompt = Rent.prompt(**kwargs)
        attr.update(**apartment_prompt, **rent_prompt)

        return attr


class ApartmentSale(Apartment, Sale, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        apartment_prompt = Apartment.prompt(**kwargs)
        sale_prompt = Sale.prompt(**kwargs)
        attr.update(**apartment_prompt, **sale_prompt)

        return attr


class HouseRent(House, Rent, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        house_prompt = House.prompt(**kwargs)
        rent_prompt = Rent.prompt(**kwargs)
        attr.update(**house_prompt, **rent_prompt)

        return attr


class HouseSale(House, Sale, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        house_prompt = House.prompt(**kwargs)
        sale_prompt = Sale.prompt(**kwargs)
        attr.update(**house_prompt, **sale_prompt)

        return attr


class StoreRent(Store, Rent, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        store_prompt = Store.prompt(**kwargs)
        rent_prompt = Rent.prompt(**kwargs)
        attr.update(**store_prompt, **rent_prompt)

        return attr


class StoreSale(Store, Sale, Advertisement):

    @classmethod
    def prompt(cls, **kwargs):
        attr = {}
        store_prompt = Store.prompt(**kwargs)
        sale_prompt = Sale.prompt(**kwargs)
        attr.update(**store_prompt, **sale_prompt)

        return attr
