import sys

from models.agent import Agent
from models.supervisor import Supervisor
from utils.utils import check_credentials, storage_to_file, load_from_file


def runner():
    if not check_credentials(sys.argv):
        print("Wrong Credentials!!!")
        return
    # Supervisor.create_agent()
    #
    # storage_to_file(file_name="agents.json", data=Supervisor.serialize_agents_list())

    agents = load_from_file(file_name="agents.json")
    agent = Agent(**agents[0])
    agent.create_estate("apartment", "rent")
    print(agent)


if __name__ == '__main__':
    runner()
