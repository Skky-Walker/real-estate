from models.base import BaseUser
from models.supervisor import Supervisor
from rules.mapper import ESTATE_MAPPER


class Agent(BaseUser):
    def __init__(self, **kwargs):
        self.estates_list = list()
        Supervisor.agents_list.append(self)
        super().__init__(**kwargs)

    @classmethod
    def prompt(cls):
        return BaseUser.prompt()

    def create_estate(self, estate, deal):
        mapper = ESTATE_MAPPER[(estate, deal)]
        instance = mapper(**mapper.prompt())
        self.estates_list.append(instance)
        Supervisor.classification_estates_dict[self].append(instance)
        return instance

    def serializer(self):
        attr = {
            "username": self.username,
            "password": self.password,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email
        }
        return attr
