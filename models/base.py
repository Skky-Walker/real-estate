from enum import Enum, unique


class BaseUser:

    def __init__(self, username, password, first_name, last_name, email, **kwargs):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls):
        username = input("Please enter your username : ")
        password = input("Please enter your password : ")
        first_name = input("Please enter your first_name : ")
        last_name = input("Please enter your last_name : ")
        email = input("Please enter your email : ")

        attr = {
            "username": username, "password": password,
            "first_name": first_name, "last_name": last_name,
            "email": email
        }
        return attr


@unique
class TypeEstate(Enum):
    office = 1
    business = 2
    residential = 3


class BaseEstate:

    def __init__(self, reseller, built_year, area, rooms_count, address, region, type_estate, **kwargs):
        self.reseller = reseller
        self.built_year = built_year
        self.area = area
        self.rooms_count = rooms_count
        self.address = address
        self.region = region
        self.type_estate = type_estate

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        built_year = input("Please enter built year estate: ")
        area = input("Please enter area estate: ")
        rooms_count = input("Please enter rooms count estate: ")
        address = input("Please enter address estate: ")
        reseller = kwargs.get("reseller")
        region = kwargs.get("region")
        type_estate = kwargs.get("type_estate")

        attr = {
            "reseller": reseller, "region": region,
            "built_year": int(built_year), "area": int(area),
            "rooms_count": int(rooms_count), "address": address,
            "type_estate": type_estate
        }
        return attr


class BaseDeal:

    def __init__(self, date, **kwargs):
        self.date = date

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        date = input("Please enter date deal: ")

        attr = {
            "date": date
        }
        return attr
