from models.base import BaseDeal
from models.supervisor import Supervisor


class Deal:

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Rent(BaseDeal, Deal):

    def __init__(self, init_price, monthly_price, discountable=False, convertable=False, **kwargs):
        self.init_price = init_price
        self.monthly_price = monthly_price
        self.discountable = discountable
        self.convertable = convertable

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        init_price = input("Please enter init_price deal: ")
        monthly_price = input("Please enter monthly_price deal: ")
        discountable = input("Please enter discountable deal: ")
        convertable = input("Please enter convertable deal: ")

        attr = {
            "init_price": int(init_price), "monthly_price": int(monthly_price),
            "discountable": bool(discountable), "convertable": bool(convertable),
        }
        attr.update(BaseDeal.prompt(**kwargs))
        return attr


class Sale(BaseDeal, Deal):

    def __init__(self, price_per_meter, discountable=False, convertable=False, **kwargs):
        self.price_per_meter = price_per_meter
        self.discountable = discountable
        self.convertable = convertable

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        price_per_meter = input("Please enter price_per_meter deal: ")
        discountable = input("Please enter discountable deal: ")
        convertable = input("Please enter convertable deal: ")

        attr = {
            "price_per_meter": int(price_per_meter),
            "discountable": bool(discountable), "convertable": bool(convertable),
        }
        attr.update(BaseDeal.prompt(**kwargs))
        return attr
