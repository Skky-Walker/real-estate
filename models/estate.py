from models.base import BaseEstate


class Apartment(BaseEstate):

    def __init__(self, floor, has_elevator, has_parking, has_balcony, **kwargs):
        self.floor = floor
        self.has_elevator = has_elevator
        self.has_parking = has_parking
        self.has_balcony = has_balcony

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        floor = input("Please enter floor estate: ")
        has_elevator = input("Please enter has elevator estate: ")
        has_parking = input("Please enter has parking estate: ")
        has_balcony = input("Please enter has balcony estate: ")

        attr = {
            "floor": int(floor), "has_elevator": bool(has_elevator),
            "has_parking": bool(has_parking), "has_balcony": bool(has_balcony),
        }
        attr.update(BaseEstate.prompt(**kwargs))
        return attr


class House(BaseEstate):

    def __init__(self, has_yard, floor_count, **kwargs):
        self.has_yard = has_yard
        self.floor_count = floor_count

        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        has_yard = input("Please enter has yard estate: ")
        floor_count = input("Please enter floor count estate: ")

        attr = {
            "has_yard": bool(has_yard), "floor_count": int(floor_count),
        }
        attr.update(BaseEstate.prompt(**kwargs))
        return attr


class Store(BaseEstate):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @classmethod
    def prompt(cls, **kwargs):
        return BaseEstate.prompt(**kwargs)
