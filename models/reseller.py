from models.supervisor import Supervisor


class Reseller:

    def __init__(self, first_name, last_name, phone_number, **kwargs):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        Supervisor.resellers_list.append(self)
        super().__init__(**kwargs)
