from collections import defaultdict

from models.base import BaseUser


class Supervisor(BaseUser):
    agents_list = list()
    estates_list = list()
    classification_estates_dict = defaultdict(list)
    resellers_list = list()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @staticmethod
    def create_agent():
        from models.agent import Agent
        agent_attr = Agent.prompt()

        instance = Agent(**agent_attr)
        return instance

    @classmethod
    def serialize_agents_list(cls):
        instances = list()

        for agent in cls.agents_list:
            serialize_data = agent.serializer()
            instances.append(serialize_data)
        return instances
