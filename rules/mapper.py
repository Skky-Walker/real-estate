from api.advertisement import (
    ApartmentRent, ApartmentSale,
    HouseRent, HouseSale
)

ESTATE_MAPPER = {
    ("apartment", "rent"): ApartmentRent,
    ("apartment", "sale"): ApartmentSale,
    ("house", "rent"): HouseRent,
    ("house", "sale"): HouseSale,
}
