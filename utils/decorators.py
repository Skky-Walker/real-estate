def check_access(func):
    def wrapper(obj, *args, **kwargs):
        if obj.__has_access:
            return func(obj, *args, **kwargs)

    return wrapper
