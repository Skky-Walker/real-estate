import json

from rules.credentials import SUPERVISOR_CREDENTIALS
from settings.settings import BASEDIR, Path


def check_credentials(prompt_args):
    if len(prompt_args) >= 3:
        _, username, password = prompt_args
        credit = {"username": username, "password": password}
        credit_check = bool(credit == SUPERVISOR_CREDENTIALS)
        return credit_check
    return False


def check_exists_file(filename):
    if not Path.exists(filename):
        Path.touch(filename)


def storage_to_file(file_name, data):
    path = f"{BASEDIR}/fixtures/{file_name}"
    check_exists_file(path)

    with open(path, "r+") as handler:
        try:
            old_json_data = json.load(handler)
            old_json_data.extend(data)
            handler.seek(0)
            json.dump(old_json_data, handler)
        except json.decoder.JSONDecodeError:
            json.dump(data, handler)


def load_from_file(file_name):
    path = f"{BASEDIR}/fixtures/{file_name}"
    check_exists_file(path)

    with open(path, "r") as handler:
        try:
            agents = json.load(handler)
        except json.decoder.JSONDecodeError:
            return []
        else:
            return agents
